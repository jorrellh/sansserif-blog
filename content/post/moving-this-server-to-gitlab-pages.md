---
title: "Moving this website to GitLab Pages"
metaAlignment: center
coverMeta: out
date: 2020-11-27
categories:
- deployment
- programming
tags:
- Hugo 
- Gitlab
---

Since the dawn of time, this website has been sitting on a simple web server hosted on DigitalOcean. However, I decided to use [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) instead. For those who aren't aware of what GitLab Pages is, it is essentially a free service for hosting static websites. GitLab sorts out all of the complex pieces of setting up a server for you so all you need to do is focus on making the website and generating the content.


# What I had before

When I first started out with this website, it was just running on DigitalOcean's servers, and I was only paying $5 USD a month. This was their cheapest option but this was actually more than enough for this website. 


## Configuration

When I first setup my server on DigitalOcean I had to:

1. Provide my public RSA key in order to SSH into the server. 
2. Install and configure NGINX to get basic web server functionality.
3. Install and configure Let's Encrypt in order to get HTTPS.
4. Copy and paste the website folder over SFTP to the server.

This wasn't bad but it isn't quite as elegant compared to how GitLab Pages does things for me which I'll get into in the next section.

## Website development with Hugo and why
This blog site was generated in Hugo which is the fastest framework for building static websites. The current theme on this website as of writing is the [tranquilpeak theme](https://themes.gohugo.io/hugo-tranquilpeak-theme/) that I found on [Hugo's themes page](https://themes.gohugo.io/), which is where you can find hundreds of themes from to use for free.

Hugo is pretty easy for making blog posts with. You just write everything in [markdown](https://en.wikipedia.org/wiki/Markdown). Then when you're ready, you just generate it with the `hugo` command and it will do the rest.

This is quite convenient for me as the other alternative I would pick would be WordPress but I don't require most of the functionality that it provides. It's also more hack proof going with Hugo as there's no potential entries into my server that could be exploited compared to WordPress. WordPress plugins are infamous for allowing this to happen if you don't update them, but I'm not interested in continuously updating stuff to keep them working if I have the choice to just not do that with no repercussions.

The only potential reason for even using WordPress that I am aware of is that I get to use a nice GUI, but I'm not fussed about GUIs. All I need is a good text editor that can open markdown and that's it.

I have also considered building my own blogging website from scratch with Rails. However, that would mean having to write everything from scratch, coming up with my own themes which would take a very long time, and also making my own backend web GUI to use which wouldn't even be of the same quality as WordPress unless I slaved away at it for weeks. It would take too long that the cost to benefit ratio wouldn't be worth it.


# What I have now

So when I first heard about GitLab Pages, I was quite surprised to hear how simple it was. It's also compatible with Hugo so it was only a matter of including the `gitlab-ci.yml` file for Hugo in my website project folder and pushing to the git repo on GitLab.

Since I already primarily use GitLab for pushing project code to, and Hugo for generating my blog site, this made perfect sense to use. GitLab Pages brought these two tools together in such a way that I appreciate. It's so elegant because all I have to do is push updates to the repo where my website is and it will take care of the rest.

As for the `gitlab-ci.yml` I mentioned, this is quite configurable and very simple. 


{{< tabbed-codeblock gitlab-cli.yml >}}
    <!-- tab yml -->
image: registry.gitlab.com/pages/hugo:latest

variables:
GIT_SUBMODULE_STRATEGY: recursive

test:
script:
    - hugo
except:
    - master

pages:
script:
    - hugo
artifacts:
    paths:
    - public
only:
    - master
    <!-- endtab -->

{{< /tabbed-codeblock >}}

Basically what this code does is pull the latest docker image called "hugo". It then copies my [project directory](https://gitlab.com/jorrellh/sansserif-blog/) into it, and then runs the hugo command to generate everything to be used on the browser. Lastly, it will spit this out into the `public` folder which GitLab will host.

I can even configure this to do other tasks as well since you can basically tell this script file to run bash commands. One of the tasks I included was to run a resize script that will convert my images into thumbnail sizes to save space when loading them on the page. 

## What about HTTPS?

Well GitLab Pages has this covered. Their UI allows for me to just simply toggle a switch that enables certificates from Let's Encrypt and their system does the rest. 

{{< image src="/images/gitlab-pages-sansserif-blog.png" title="GitLab Pages UI showing this blog's config" >}}

# Closing

So far I am quite pleased with how elegant and simple GitLab Pages is for hosting static content. I don't have to worry about purchasing a VPS and I optionally don't even need a domain name since I can just use [https://jorrellh.gitlab.io/sansserif-blog/](https://jorrellh.gitlab.io/sansserif-blog/) instead. Although for my case, I will still be keeping my domain name just because I have an email server configured to use it. Though, for anyone else, this is a pretty good deal all for the price of free!

I am especially happy with how easy it is to update my website with GitLab CI rather than manually copying and pasting things across via SFTP to the server. This has simplified the process. I don't think I will be switching away from this any time soon. It also means I have the ability to close down my server on DigitalOcean and save money. 

If you have a simple static website that you pay to host somewhere, you may wish to consider this as a potential option to use instead. GitLab Pages will work perfectly fine for this use case. As for me, I use Hugo for generating blog posts easily and quickly but you don't need this to use GitLab Pages if you don't want it. GitLab Pages is free and simple to use so you don't even need to pay for a VPS.