---
title: "An Idea: Creating my own fitness tracker"
thumbnailImage: /images/arduino-uno.jpg
metaAlignment: center
coverMeta: out
date: 2020-06-30
categories:
- electronics
- programming
- GNSS
tags:
- EasyEDA 
- Arduino
- GPS
---

# Backstory
Recently I started getting into electronics. I got set up using an Arduino and figured I could make something useful with it. At the time I was tinkering with the Arduino, I had a LAN adblocker. Essentially blocking ads on every device on my local network. I was using pihole, and it had an open API that allowed me to request statistics about how many ads it was blocking. 

I ended up creating a rather simple program on the arduino that would fetch this information using the WiFi module and would display it to a small LCD display. I was able to get information every so often from the server about how many ads were getting blocked which was pretty neat. But this was only just to test out using the Arduino. 

{{< image src="/images/adblocker-arduino-lcd.jpg" title="I2C LCD Display showing adblocker statistics" >}}


# My idea
Now fast forward to a few months later, I got an idea in my head about making a fitness tracker that uses a GPS module to record GPS logs to a GPX file and save it to an SD card. The idea is so you can record your fitness activity like running or cycling and upload it to a website like [Strava](https://www.strava.com).  About a week later, I started becoming interested in custom made electronic circuits. Then it hit me. What if I created my own fitness tracker on a custom PCB? 

It could be a little device which has just a simple button to press to start and stop recording and a little light indicator informing you on if it's recording. If I can create a custom PCB designed for fitness tracking, then I can 3D print a case for it and slap a battery in it. 

{{< alert info >}}
As of writing, I haven’t given an internal battery much thought, so for now we’re focused on an external battery as it’s the easiest, but having an internal battery would make things more convenient and that will remain the ideal feature for me to include inside of the case.
{{< /alert >}}

When I'm looking into getting an internal battery, I would like it to last a long time. I think 10 hours should be good enough, but we will see how we go.

# The plan

## Prototyping with development hardware

### The hardware

{{< image src="/images/arduino-uno.jpg" title="Arduino Uno board" >}}

I need to make it first using the Arduino so I can test out my idea and work on a prototype. This involves using a breadboard to connect the various chips I need in order to get this to work on the hardware level. What parts exactly?

To sum it up, I need the following:
* An Arduino Uno (ATmega328P)
* A GPS module (GY-GPS6MV2)
* An SD Card module
* A breadboard
* An LED light (Might be unnecessary I think as the GPS module has one already)
* A button to start and stop recording an activity
* An external battery
* And perhaps other various pieces to connect these together.

### The software
On the software side, since I’m just using the Arduino, I only need the Arduino IDE. This conveniently gives me access to hundreds of libraries which I’ll be using to interface with the GPS and SD Card module. It will also aid me with being able to save the GPS coordinates to a GPX file.

Once I’ve got the hardware and the software side of things ready at the prototype stage, there exist another hurdle to go through, however, assuming I get the prototype working correctly, the next part shouldn’t be too difficult. I will be using EasyEDA which will aid in the design of the schematic and the PCB layout. 

## Designing the schematic, the PCB, and the case
Essentially all I need to do from here after is to create a copy of the prototype hardware schematic that is albeit, much more slimmed down. Once I’ve created the schematic, I can then start to design the PCB. I believe this will be more tricky as I will need to come up with a design, as well as a 3D printed case design, and then compromise depending on the physical limitations of the chips involving the traces and placement of everything. But the goal is to make it small enough so it can easily fit inside a pocket.

Then I need to generate the gerber files and then order the PCB by companies to print and etch the traces according to the design before shipping them to me. I will also need to order the parts I need to solder on to the PCB. Once that's done, I can then flash the software onto the chip and then it should run like the prototype.

If this goes over your head, don't worry as we will explain this is more detail in my next posts when we get to go into the nitty gritty details. This is really just an overview of the process.

# Should I be doing this?
Well it depends on how you approach the question. For example, should I be doing this to learn? Should I be doing this to create something unique? Should I be doing this because it’s fun? 
I think having a wide range of skills in computers is very useful. I have knowledge in the software side of things but I don’t know a whole lot about the hardware side. I believe learning the process in creating a custom PCB will be incredibly useful if I choose to make something that is actually useful assuming what I am currently making isn’t and cannot be improved upon either to be made useful.
I can certainly tell you that this is rather fun. Tinkering with stuff is always fun for me. When my GPS module arrives, I can start working on this immediately. Whether this is useful or not is irrelevant to me and only adds an extra layer of coolness on top of the fun and the learning.

# My predictions
I will no doubt face multiple different hurdles along the way, and I may even have moments where I did not consider something that I will need further down the line. Since it is nothing like what I’ve done before, I don’t know what to expect. However, I am happy to go back and revise to make things better and learn from it.

# Supplemental ideas
I’ve also got a few other ideas buzzing around in my head over this. Here are some ideas I think will add to the core functionality but are not required. Think of these as just cool bonuses.

### Adding a display
I am thinking about adding a display, but I have to keep in mind that since the battery life is key, I want the battery life to last for roughly 10 hours. Displays quite often use more power than anything else on the board so I need to be conscientious about this. I also need to consider a design for the display and what size it should be. I’m not fussed about colours at this stage so that also allows me to be flexible with choosing the display types as they all display things differently. These are what I can use:

#### OLED Display
An OLED screen is quite efficient as there is no backlight like what an LCD displays use. Each LED lights up itself, thus anything black on the screen is essentially saving power as it is not turned on. However, the downsides to OLED is that there is burn in, though from personal experience, I haven’t personally had burn in problems. Since I’m not really fussed about longevity though, I am happy with using OLED anyway for the battery life benefits.

#### LCD Display
This is the most common form of display. However, LCD displays rely on a backlight and can use up more battery life. They do not have burn in problems, so they can last a very long time. I can technically use this if I add buttons to control the brightness, but ultimately I may only consider this if longevity is key and if I feel that losing some battery life is worth it.

#### E-paper Display
These displays are uncommon, but they use essentially no power, and they only require it when necessary to refresh the screen. Since the display is really just for displaying information, this is an ultimate win unless I need to refresh it a bunch of times, which is also it’s limitation. It has a significantly worse refresh rate compared to the other displays. These displays are also apparently more difficult to develop with compared to the other displays I mentioned. However, I am rather intrigued by this display technology and I may attempt to get it working alongside the OLED one.

### Higher quality case
In case I feel that a 3D print design isn’t good enough, I can also resort to getting a plastic injection moulded case which will significantly improve the look of the case. You can see why this idea is put in the supplemental category as it is not a requirement.

But that could cost more money than what it's worth, and unless I plan on mass producing these things (I don't), then I'm just gonna 3D print it and sand the faces of the case to give it that smooth look. Heck, I'll even paint it too if it makes it look even nicer.

# The next step
For now I am waiting on the GPS module to arrive. Then I can get started working on this. I will post an update once I have made the prototype work on the Arduino along with some photos. Stay tuned!~