---
title: "Cartridge razors are awful but here's a much better solution"
thumbnailImage: /images/double-edge-safety-razor.jpg
metaAlignment: center
coverMeta: out
date: 2020-11-28
categories:
- health & beauty
tags:
- razor blades
---

{{< image src="/images/double-edge-safety-razor.jpg" title="Double edge safety razor next to a cartridge razor" >}}

If you walk into your local supermarket, you will no doubt find a pack of cartridge razors everywhere in the health and beauty aisle. However, it didn't used to look like this. Decades ago, these were not the most popular forms to exist. You might not have given it much thought but these cartridge razors aren't the best design either. Let's walk through what a cartridge razor actually is and how it compares to a less common but superior design, the double edge safety razor. Just for simplicity sakes, I'll be referring to double edge safety razors as DE razors. 

A cartridge razor typically has more than two but no more than five blades attached to it. These are marketed as being the successor to DE razors which only has one blade on each side. They are also slightly more ergonomic to use. However, these razors have a few fatal flaws.

{{< image src="/images/gillette-fusion.webp" title="Gillette Fusion bundled with three cartridges (very expensive!)" >}}

## Vendor lock-in

Firstly, each brand of cartridge razors use their own proprietary cartridge. If you buy Schick, only cartridges made by that company will work on their own. Same with BIC, Gillette, and many others. You cannot decide to purchase another brand's cartridge from say, BIC, and use that in your Gillette Fusion cartridge razor. This means that each company can essentially charge you as much as they want for their own cartridges because no one can compete when there is no standard to compete on. They are all making their own standard which is why they are so much more expensive.

This is vastly different with DE razors. For a pack of 50 blades, it only costs roughly 10 dollars depending on where you're buying it from. Cartridge razors on the other hand exceed this cost by a lot! The lowest I've seen is a pack of 4 costing about 20 dollars! And a pack of 8 for about $50. Just for comparison, DE razor blades for that price could easily yield you close to a pack of 100. Best of all, you can use any razor blade for any DE razor. They're all compatible with each other because they all share the same standard for the blade design. 

These razor blades in the following image are made by Feather. These are very high quality blades that are quite sharp. These came in a pack of 50 and I only paid roughly $20 for it!

{{< image src="/images/razor-blades.jpg" title="Feather DE Razor Blades (pack of 10)" >}}


## More blades are not better
What's worse, these companies market their razors as having more blades. But does anyone know why having more blades is better and why they should care if one has 5 blades instead of 4? The reason is because the more blades, the more hair it can cut below the skin. However, this is actually quite bad. Having more blades is the worst.

When you have multiple blades on a razor, you increase your risk of getting skin irritation because more blades are hitting the hairs, then the skin, all at once. Yes it does shave the hairs off better, but it can also cut too much below the skin which will make it easier to get ingrown hair cysts. This is when the hair is trying to grow out from the skin but it cannot because it's trapped under it. DE razors do no such thing because there is only one blade. One blade cuts the hairs to be flush with the skin (I think they give me a much closer shave anyway!), which in turn drastically decreases the risk of skin infection and avoids the risk of getting ingrown hairs.

Hairs also gets caught in the cartridge far easier which essentially makes them defective by design. You buy them, start using them, and then the hairs gets stuck as you're using them and so you have to try to wash them off. You can sort of get them out, but it's not very easy because the more blades there are, the easier it is for hairs to get stuck in between them. DE razors literally cannot and do not have this problem by design because there is only one blade. I haven't had any problem getting hairs out of my razor at all! 

## They are not as environmentally friendly

When you buy a cartridge razor, many of them have interchangeable cartridges. Some don't if you get cheaper ones. The interchangeables ones are mildly convenient because when you wear it out, you can just buy another pack of cartridges so you don't have to throw out the whole razor. This sounds better for the environment, doesn't it? Well not so fast. Let's go over why.

Cartridge razors are often made from plastic instead of metal which means that they can break easier because they are weaker and they are also not bio-degradeable. You could recycle them, but most people just chuck their razors in the bin and buy a new one. Even if you're using replaceable parts, they are also often made from plastic as well and they end up getting chucked out too. (Though it's more common for people to not throw their last cartridge out because they're so expensive, some people would rather deal with the blunt razors than get a new one.)

However, as far as I'm aware, Gillette has made some effort to fix this waste issue by partnering up with [TerraCycle](https://www.terracycle.com/en-US/brigades/gillette) to help aid in recycling people's used cartridge razor although the amount of people who actually know that this exists is very slim so you would have to be a very environmentally conscious person to go out of your way to find out this information. 

Sure, DE razors can also be made from plastic but they're not as common to find. I have never seen one personally, but the amount of plastic cartridge razors I've seen is too many and even the expensive ones still have bits of plastic. Unlike these cartridge razor blades, the blades in the DE razors can be easily disposed. You can even sharpen them if you want to increase the lifespan which you can't do with the other ones!

## It costs more in the long term
I mentioned earlier that the cost of cartridge razors are significantly more expensive than just regular DE razors. In the long term, you would be spending a lot more money on them. But by how much is it cheaper than cartridge razors to use DE razors?

I'll show you. 

{{< alert info >}}
Note: The prices in the following example are based off of my own findings from browsing Coles and Woolworths. The prices you find may vary slightly at the store you shop at but the amount these cost will still be a lot compared to DE razors!
{{< /alert >}}

> Andy decides to purchase an interchangeable cartridge razor. He goes to the store to purchase a cartridge razor for about $20. The standard price for a pack of 8 for this razor design is about $45.

> Brad decides to purchase a disposable cartridge razor that is store brand, so he spends about $3 on a pack of 15 cheap twin blade razors. He also uses it the same amount as Andy.

> Chad decides to get a DE razor. He buys a fairly decent one for roughly $50 that's a bit on the pricier side. He too uses his razor the same amount as the other guys.

So one year has gone by. Now who has spent the most?

||Andy|Brad|Chad|
|---|---|---|---|
|Razor type|interchangeable cartridge|disposable cartridge|double edge|
|Initial cost|$20|$3|$50|
|Blades/cartridges cost|$45|$3|$10|
|Blades/cartridges gained|8|15|50|
|Amount spent on refills in a year|$270 with 4 weeks remaining|$12 with 8 weeks remaining|$20 with 48 weeks remaining|
|Total after one year|$290|$12|$70|

As you can see, Chad with his DE razor only paid less than 1/4th the price compared to the interchangeable cartridge designs with 48 weeks left compared to the measely 4 weeks for Andy. Brad got the cheap disposable ones which are quite literally the cheapest ones to get, although, if you get those, it would be a significantly worse experience compared to even the interchangeable ones that Andy got. So unless you want to cheap out like crazy, be like Chad. He is smart with money and got the best shave every single day for a year and still saved more than what Andy did.


## Helpful chart
So that's a bunch that we've covered already. I felt like creating this chart to easily visualise and break down what was discussed, detailing the specific features these two designs have.

|Feature|Cartridge razors|DE razors|
|---|---|---|
|Ability to use blades/cartridges from other companies|No|Yes|
|No. of blades pressed against skin|Up to five|One|
|Price range|roughly $3-50|$20-100+|
|Price of replaceable blade (pack of 50)|roughly $50 (pack of 8 only)|roughly $10 for pack of 50|

## So why aren't double edge safety razors more common then?

Well it comes down to marketing. Companies market cartridge razors as being the greatest thing since sliced bread that it's no wonder they are so common. They will say that more blades are better, however, we've already established earlier that this is not true. However, Gillette's PR is trying to convince people that their skin irritation is due to them not washing or moisturizing their face. No seriously, [I'm not making this up.](https://www.gillette.com.au/en-au/shaving-tips/how-to-shave/razor-bumps) Because you know, the bigger the number, the better it is right? But this is Gillette and they obviously want to sell their products so it's not hard to understand why they would say this. But regardless, you can search for any article proving to the contrary about this claim. Here's one example of an article from [Men's Health](https://www.menshealth.com/style/a19526899/single-blade-razor-shave/) about a guy who transitioned to using a DE razor after dealing with this exact same issue of irritation and ingrown hairs. Personally for me, I don't even deal with the same level of skin irritation with these razors either compared to my old Gillette Fusion cartridge razor.

However, there is one convenience factor to cartridge razors and that is they are easier to learn how to use. You simply go in one direction and it shaves. However, the DE razors are less forgiving towards beginners and can cut you easily as there is some learning curve to it, however, the cut isn't very deep at all and once you get used to the angle to hold it in, it becomes a no brainer.

## Should you get a double edge razor instead?
There is no question that these are the best razors you can get. Cartridge razors are expensive, not good for your skin, and contribute far more to waste than DE razors. That being said, there is a slight learning curve to using them and if you're not experienced with them, you can cut yourself accidentally. Fortunately the cuts that I've had when first using them are quite small and nothing dramatic. 

If you deal with skin irritation and bumps when using a cartridge razor like I used to, then you may find it worth the jump to try these ones out instead. 

If you like to budget your money and want to save money long term, then you can still get a fairly cheap DE razor that is much better for your skin still and has a less upfront cost. You'll save even more money long term too so there is basically no reason to *not* get these.

If you care about the environment, you should also consider getting these as well as you can get some that are entirely made of steel. 

The only times I don't agree with getting this kind of razor, or any razor in general really is if you don't want to shave. If you are happy with your hair then more power to you! 

## Where can you get these?
Unfortunately though, it seems as though this trend of cartridge razors won't be going any time soon. It would take bringing awareness about this to the public and informing people about the benefits. So for now, it looks like it is here to stay for good. There aren't even many stores that sell DE razors either. So unless there is a shaver shop nearby or your supermarket sells them, then your best bet is to buy them online as they are still easy to find on websites such as Amazon, eBay, or somewhere else. 








