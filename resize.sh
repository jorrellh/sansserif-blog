#!/bin/bash
cp -r static/images/ static/originals/
mogrify -resize 140x140 static/images/*.jpg
mogrify -resize 140x140 static/images/*.png

for f in static/images/*.jpg; do
    mv "$f" "${f%.jpg}_thumb.jpg"
done

for f in static/images/*.png; do
    mv "$f" "${f%.png}_thumb.png"
done

cp -r static/originals/* static/images/

rm -rf static/originals
